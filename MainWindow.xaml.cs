﻿using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;
using Fizbin.Kinect.Gestures;

namespace LookAtMe
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Microsoft.Kinect;
    using Microsoft.Samples.Kinect.SwipeGestureRecognizer;

    /// <summary>
    /// Klasa głównego okna programu
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Recognizer
        /// </summary>
        private readonly Recognizer activeRecognizer;


        /// <summary>
        ///  Obiekt służący do rozpozanawania gestów
        /// </summary>
        private GestureController gestureController;

        /// <summary>
        /// Tryb opisu slajdu
        /// </summary>
        private Boolean descriptionModule = false;

        /// <summary>
        /// Ścieżki do plików obrazów
        /// </summary>
        private string[] picturePaths = CreatePicturePaths();

        /// <summary>
        /// Tytuły slajdów
        /// </summary>
        private List<String> titles = new List<String>();

        /// <summary>
        /// Opisy slajdów
        /// </summary>
        private List<String> descriptions = new List<String>();

        /// <summary>
        /// Informacja o kolorze
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Obraz z kamery
        /// </summary>
        private byte[] colorPixels;

        /// <summary>
        /// Tablica wielowymarowa zawierająca cały szkielet postaci
        /// </summary>
        private static readonly JointType[][] SkeletonSegmentRuns = new JointType[][]
        {
            new JointType[] 
            { 
                JointType.Head, JointType.ShoulderCenter, JointType.HipCenter 
            },
            new JointType[] 
            { 
                JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft,
                JointType.ShoulderCenter,
                JointType.ShoulderRight, JointType.ElbowRight, JointType.WristRight, JointType.HandRight
            },
            new JointType[]
            {
                JointType.FootLeft, JointType.AnkleLeft, JointType.KneeLeft, JointType.HipLeft,
                JointType.HipCenter,
                JointType.HipRight, JointType.KneeRight, JointType.AnkleRight, JointType.FootRight
            }
        };

        /// <summary>
        /// Sensor Kinecta
        /// </summary>
        private KinectSensor nui;

        /// <summary>
        /// Stan podłączenia Kinecta
        /// </summary>
        private bool isDisconnectedField = true;

        /// <summary>
        /// Wiadomość o połączeniu Kinecta
        /// </summary>
        private string disconnectedReasonField;

        /// <summary>
        /// Tablica zawierająca rozpoznane szkielety. (Analiza tylko pierwszego)
        /// </summary>
        private Skeleton[] skeletons = new Skeleton[0];

        /// <summary>
        /// Czas podświetlenia szkieletu na kamerze RGB
        /// </summary>
        private DateTime highlightTime = DateTime.MinValue;

        /// <summary>
        /// ID podświetlonego szkieletu
        /// </summary>
        private int highlightId = -1;

        /// <summary>
        /// ID śledzonego szkieletu
        /// </summary>
        private int nearestId = -1;

        /// <summary>
        /// Index aktualnego slajdu
        /// </summary>
        private int indexField = 1;

        /// <summary>
        /// Inicjalizacja nowej instancji <see cref="MainWindow" /> klasy.
        /// </summary>
        ///
        public MainWindow()
        {

            this.PreviousPicture = this.LoadPicture(this.Index - 1);
            this.Picture = this.LoadPicture(this.Index);
            this.NextPicture = this.LoadPicture(this.Index + 1);

            titles.Add("Title");
            descriptions.Add("Sample description");

            InitializeComponent();

            titles.Add("Title");
            descriptions.Add("Sample description");

            // Rozpoznawanie gestów
            this.activeRecognizer = this.CreateRecognizer();

            // Podłączenie eventu
            Loaded += this.OnMainWindowLoaded;
        }


        /// <summary>
        /// Event INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sprawdzenie czy kinect jest podłączony
        /// </summary>
        public bool IsDisconnected
        {
            get
            {
                return this.isDisconnectedField;
            }

            private set
            {
                if (this.isDisconnectedField != value)
                {
                    this.isDisconnectedField = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("IsDisconnected"));
                    }
                }
            }
        }

        /// <summary>
        /// Wiadomość związana z brakiem kinecta
        /// </summary>
        public string DisconnectedReason
        {
            get
            {
                return this.disconnectedReasonField;
            }

            private set
            {
                if (this.disconnectedReasonField != value)
                {
                    this.disconnectedReasonField = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("DisconnectedReason"));
                    }
                }
            }
        }

        /// <summary>
        /// Getter i setter dla indexu slajdu.
        /// </summary>
        public int Index
        {
            get
            {
                return this.indexField;
            }

            set
            {
                if (this.indexField != value)
                {
                    this.indexField = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("Index"));
                    }
                }
            }
        }

        /// <summary>
        /// Poprzedni slajd
        /// </summary>
        public BitmapImage PreviousPicture { get; private set; }

        /// <summary>
        /// Obecny Slajd
        /// </summary>
        public BitmapImage Picture { get; private set; }

        /// <summary>
        /// Następny slajd
        /// </summary>
        public BitmapImage NextPicture { get; private set; }

        /// <summary>
        /// Lista plików do wyświetlenia jako slajd
        /// </summary>
        /// <returns>Ścieżki obrazów</returns>
        private static string[] CreatePicturePaths()
        {
            var list = new List<string>();

            var commonPicturesPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonPictures);
            Console.WriteLine(commonPicturesPath);
            list.AddRange(Directory.GetFiles(commonPicturesPath, "*.jpg", SearchOption.AllDirectories));
            if (list.Count == 0)
            {
                list.AddRange(Directory.GetFiles(commonPicturesPath, "*.png", SearchOption.AllDirectories));
            }

            if (list.Count == 0)
            {
                var myPicturesPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                list.AddRange(Directory.GetFiles(myPicturesPath, "*.jpg", SearchOption.AllDirectories));
                if (list.Count == 0)
                {
                    list.AddRange(Directory.GetFiles(commonPicturesPath, "*.png", SearchOption.AllDirectories));
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// Załadowanie obrazu o podanym indexie
        /// </summary>
        /// <param name="index">Index obrazka do załadowania</param>
        /// <returns>Obrazek o podanym indexie</returns>
        private BitmapImage LoadPicture(int index)
        {
            BitmapImage value;

            if (this.picturePaths.Length != 0)
            {
                var actualIndex = index % this.picturePaths.Length;
                if (actualIndex < 0)
                {
                    actualIndex += this.picturePaths.Length;
                }

                Debug.Assert(0 <= actualIndex, "Index used will be non-negative");
                Debug.Assert(actualIndex < this.picturePaths.Length, "Index is within bounds of path array");

                try
                {
                    value = new BitmapImage(new Uri(this.picturePaths[actualIndex]));
                }
                catch (NotSupportedException)
                {
                    value = null;
                }
            }
            else
            {
                value = null;
            }

            return value;
        }

        /// <summary>
        /// Rozpoznawanie ruchów ręki lewo i prawo (przesunięcia slajdów)
        /// </summary>
        /// <returns>Recognizer</returns>
        private Recognizer CreateRecognizer()
        {

            var recognizer = new Recognizer();



            // Prawa ręka
            recognizer.SwipeRightDetected += (s, e) =>
            {
                if (e.Skeleton.TrackingId == nearestId && !descriptionModule)
                {
                    Index++;


                    var actualIndex = Index % this.titles.Count;
                    if (actualIndex < 0)
                    {
                        actualIndex += this.titles.Count;
                    }

                    this.titleTextBlock.Text = titles[actualIndex];
                    this.descriptionTextBox.Text = descriptions[actualIndex];

                    this.PreviousPicture = this.Picture;
                    this.Picture = this.NextPicture;
                    this.NextPicture = LoadPicture(Index + 1);

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("PreviousPicture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("Picture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("NextPicture"));
                    }

                    var storyboard = Resources["LeftAnimate"] as Storyboard;
                    if (storyboard != null)
                    {
                        storyboard.Begin();
                    }

                    HighlightSkeleton(e.Skeleton);
                }
            };

            // Lewa ręka
            recognizer.SwipeLeftDetected += (s, e) =>
            {
                if (e.Skeleton.TrackingId == nearestId && !descriptionModule)
                {
                    Index--;


                    var actualIndex = Index % this.titles.Count;
                    if (actualIndex < 0)
                    {
                        actualIndex += this.titles.Count;
                    }

                    this.titleTextBlock.Text = titles[actualIndex];
                    this.descriptionTextBox.Text = descriptions[actualIndex];

                    this.NextPicture = this.Picture;
                    this.Picture = this.PreviousPicture;
                    this.PreviousPicture = LoadPicture(Index - 1);

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("PreviousPicture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("Picture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("NextPicture"));
                    }

                    var storyboard = Resources["RightAnimate"] as Storyboard;
                    if (storyboard != null)
                    {
                        storyboard.Begin();
                    }

                    HighlightSkeleton(e.Skeleton);
                }
            };

            return recognizer;
        }

        /// <summary>
        /// Początkowa inicjalizacja kontrolera Kinect
        /// </summary>
        private void InitializeNui()
        {
            this.UninitializeNui();

            var index = 0;
            while (this.nui == null && index < KinectSensor.KinectSensors.Count)
            {
                try
                {
                    this.nui = KinectSensor.KinectSensors[index];

                    this.nui.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

                    this.colorPixels = new byte[this.nui.ColorStream.FramePixelDataLength];

                    this.colorBitmap = new WriteableBitmap(this.nui.ColorStream.FrameWidth, this.nui.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

                    this.Image.Source = this.colorBitmap;

                    this.nui.ColorFrameReady += this.SensorColorFrameReady;

                    this.nui.Start();

                    this.IsDisconnected = false;
                    this.DisconnectedReason = null;
                }
                catch (IOException ex)
                {
                    this.nui = null;

                    this.DisconnectedReason = ex.Message;
                }
                catch (InvalidOperationException ex)
                {
                    this.nui = null;

                    this.DisconnectedReason = ex.Message;
                }

                index++;
            }

            if (this.nui != null)
            {
                this.nui.SkeletonStream.Enable();

                this.nui.SkeletonFrameReady += this.OnSkeletonFrameReady;

                gestureController = new GestureController();

                gestureController.GestureRecognized += OnGestureRecognized;

            }



        }

        /// <summary>
        /// Odłączenie Kinecta
        /// </summary>
        private void UninitializeNui()
        {
            if (this.nui != null)
            {
                this.nui.SkeletonFrameReady -= this.OnSkeletonFrameReady;

                this.nui.Stop();

                this.nui = null;
            }

            this.IsDisconnected = true;
            this.DisconnectedReason = null;
        }

        /// <summary>
        /// Event załadowania głównego okna programu
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void OnMainWindowLoaded(object sender, RoutedEventArgs e)
        {
            // inicjalizuj kinecta
            this.InitializeNui();

            // podpięcie eventu zmiany stanu
            KinectSensor.KinectSensors.StatusChanged += (s, ee) =>
            {
                switch (ee.Status)
                {
                    case KinectStatus.Connected:
                        if (nui == null)
                        {
                            Debug.WriteLine("New Kinect connected");

                            InitializeNui();
                        }
                        else
                        {
                            Debug.WriteLine("Existing Kinect signalled connection");
                        }

                        break;
                    default:
                        if (ee.Sensor == nui)
                        {
                            Debug.WriteLine("Existing Kinect disconnected");

                            UninitializeNui();
                        }
                        else
                        {
                            Debug.WriteLine("Other Kinect event occurred");
                        }

                        break;
                }
            };
        }

        /// <summary>
        /// Event gotowości szkieletu
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void OnSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {

            using (var frame = e.OpenSkeletonFrame())
            {
                if (frame != null)
                {

                    if (this.skeletons.Length != frame.SkeletonArrayLength)
                    {
                        this.skeletons = new Skeleton[frame.SkeletonArrayLength];
                    }

                    frame.CopySkeletonDataTo(this.skeletons);


                    var newNearestId = -1;
                    var nearestDistance2 = double.MaxValue;


                    foreach (var skeleton in this.skeletons)
                    {

                        if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                        {

                            var distance2 = (skeleton.Position.X * skeleton.Position.X) +
                                (skeleton.Position.Y * skeleton.Position.Y) +
                                (skeleton.Position.Z * skeleton.Position.Z);


                            if (distance2 < nearestDistance2)
                            {
                                newNearestId = skeleton.TrackingId;
                                nearestDistance2 = distance2;
                            }

                            gestureController.UpdateAllGestures(skeleton);
                        }


                    }

                    if (this.nearestId != newNearestId)
                    {
                        this.nearestId = newNearestId;
                    }

                    // przenieś szkielet do recognizera w celu wyrkycia gestów
                    this.activeRecognizer.Recognize(sender, frame, this.skeletons);

                    this.DrawStickMen(this.skeletons);
                }


            }



        }


        /// <summary>
        /// Rozpoznanie gestów
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Gesture event.</param>
        private void OnGestureRecognized(object sender, GestureEventArgs e)
        {
            Debug.WriteLine(e.GestureType);
            String gest = "nothing";
            Console.WriteLine(e.TrackingId);
            if (e.TrackingId == this.nearestId)
                switch (e.GestureType)
                {
                    case GestureType.Menu:
                        gest = "Menu";
                        break;
                    case GestureType.WaveRight:
                        gest = "Wave Right";
                        break;
                    case GestureType.WaveLeft:
                        gest = "Wave Left";
                        break;
                    case GestureType.JoinedHands:
                        gest = "Joined Hands";
                        break;
                    case GestureType.SwipeLeft:
                        gest = "Swipe Left";
                        break;
                    case GestureType.SwipeRight:
                        gest = "Swipe Right";
                        break;
                    case GestureType.ZoomIn:
                        gest = "Zoom In";
                        descriptionModule = true;
                        titleTextBlock.Visibility = Visibility.Visible;
                        descriptionTextBox.Visibility = Visibility.Visible;
                        break;
                    case GestureType.ZoomOut:
                        gest = "Zoom Out";
                        descriptionModule = false;
                        titleTextBlock.Visibility = Visibility.Hidden;
                        descriptionTextBox.Visibility = Visibility.Hidden;
                        break;

                    default:
                        break;
                }

            Console.WriteLine(gest);
        }


        /// <summary>
        /// Wybór szkieletu, który ma zostać podświetlony
        /// </summary>
        /// <param name="skeleton">Szkielet</param>
        private void HighlightSkeleton(Skeleton skeleton)
        {
            this.highlightTime = DateTime.UtcNow + TimeSpan.FromSeconds(0.5);
            this.highlightId = skeleton.TrackingId;
        }

        /// <summary>
        /// Rysuj szkielet rozpoznanej osoby
        /// </summary>
        /// <param name="skeletons">Szkielet osoby.</param>
        private void DrawStickMen(Skeleton[] skeletons)
        {
            //Wyczyść
            StickMen.Children.Clear();

            foreach (var skeleton in skeletons)
            {
                if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                {
                    this.DrawStickMan(skeleton, Brushes.WhiteSmoke, 7);
                }
            }

            foreach (var skeleton in skeletons)
            {
                if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                {
                    var brush = DateTime.UtcNow < this.highlightTime && skeleton.TrackingId == this.highlightId ? Brushes.Red :
                        skeleton.TrackingId == this.nearestId ? Brushes.Black : Brushes.Gray;

                    this.DrawStickMan(skeleton, brush, 3);
                }
            }
        }

        /// <summary>
        /// Funkcja rysująca szkielet
        /// </summary>
        /// <param name="skeleton">Szkielet osoby.</param>
        /// <param name="brush">Typ pędzla.</param>
        /// <param name="thickness">Grubość szkieletu jaki ma być narysowany.</param>
        private void DrawStickMan(Skeleton skeleton, Brush brush, int thickness)
        {
            Debug.Assert(skeleton.TrackingState == SkeletonTrackingState.Tracked, "The skeleton is being tracked.");

            foreach (var run in SkeletonSegmentRuns)
            {
                var next = this.GetJointPoint(skeleton, run[0]);
                for (var i = 1; i < run.Length; i++)
                {
                    var prev = next;
                    next = this.GetJointPoint(skeleton, run[i]);

                    var line = new Line
                    {
                        Stroke = brush,
                        StrokeThickness = thickness,
                        X1 = prev.X,
                        Y1 = prev.Y,
                        X2 = next.X,
                        Y2 = next.Y,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeStartLineCap = PenLineCap.Round
                    };

                    StickMen.Children.Add(line);
                }
            }
        }

        /// <summary>
        /// Przetransformuj stawy szkieletu do poszczególnych punktów
        /// </summary>
        /// <param name="skeleton">Szkielet.</param>
        /// <param name="jointType">Typ stawu.</param>
        /// <returns>Punkt na wynikowym szkielecie do rysowania.</returns>
        private Point GetJointPoint(Skeleton skeleton, JointType jointType)
        {
            var joint = skeleton.Joints[jointType];

            var point = new Point
            {
                X = (StickMen.Width / 2) + (StickMen.Width * joint.Position.X / 2),
                Y = (StickMen.Height / 2) - (StickMen.Height * joint.Position.Y / 2)

            };

            return point;
        }

        /// <summary>
        /// Event gotowości ramki kamery.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    colorFrame.CopyPixelDataTo(this.colorPixels);

                    this.colorBitmap.WritePixels(
                        new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                        this.colorPixels,
                        this.colorBitmap.PixelWidth * sizeof(int),
                        0);
                }
            }
        }
        /// <summary>
        /// Obsługa slajdów za pomocą przycisków myszki (Przesunięcie slajdów w lewo) 
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void current_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Index++;

            this.PreviousPicture = this.Picture;
            this.Picture = this.NextPicture;
            this.NextPicture = LoadPicture(Index + 1);

            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs("PreviousPicture"));
                this.PropertyChanged(this, new PropertyChangedEventArgs("Picture"));
                this.PropertyChanged(this, new PropertyChangedEventArgs("NextPicture"));
            }

            var storyboard = Resources["LeftAnimate"] as Storyboard;
            if (storyboard != null)
            {
                storyboard.Begin();
            }
        }

        /// <summary>
        /// Obsługa slajdów za pomocą przycisków myszki (Przesunięcie slajdów w prawo)
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void current_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Index--;

            this.NextPicture = this.Picture;
            this.Picture = this.PreviousPicture;
            this.PreviousPicture = LoadPicture(Index - 1);

            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs("PreviousPicture"));
                this.PropertyChanged(this, new PropertyChangedEventArgs("Picture"));
                this.PropertyChanged(this, new PropertyChangedEventArgs("NextPicture"));
            }

            var storyboard = Resources["RightAnimate"] as Storyboard;
            if (storyboard != null)
            {
                storyboard.Begin();
            }
        }

        /// <summary>
        /// Obsługa przycisku wyboru ścieżki do plików XML
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void imgPathButton_Click(object sender, RoutedEventArgs e)
        {

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();


            if (result == System.Windows.Forms.DialogResult.OK)
            {

                var list = new List<string>();
                titles = new List<String>();
                descriptions = new List<String>();
                var commonPicturesPath = fbd.SelectedPath;
                Console.WriteLine(commonPicturesPath);
                var xmlPath = Directory.GetFiles(commonPicturesPath, "*.xml", SearchOption.AllDirectories)[0];

                XmlTextReader reader = new XmlTextReader(new StreamReader(xmlPath, Encoding.GetEncoding("UTF-8")));

                while (reader.Read())
                {

                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "imageFile":
                                var imagePath = fbd.SelectedPath + "\\" + reader.ReadString();
                                list.Add(imagePath);
                                Console.WriteLine("imageFile: " + imagePath);
                                break;
                            case "title":
                                String title = reader.ReadString();
                                titles.Add(title);
                                Console.WriteLine("title: " + title);
                                break;
                            case "description":
                                String description = reader.ReadString();
                                descriptions.Add(description);
                                Console.WriteLine("description: " + description);
                                break;
                        }
                    }



                }
                Console.ReadLine();
                picturePaths = list.ToArray();

                var actualIndex = Index % this.titles.Count;
                if (actualIndex < 0)
                {
                    actualIndex += this.titles.Count;
                }

                this.titleTextBlock.Text = titles[actualIndex];
                this.descriptionTextBox.Text = descriptions[actualIndex];
            }


        }
        /// <summary>
        /// Event obsługujący Fullscreen okna (Włączenie trybu Fullscreen)
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void fullButton_Click(object sender, RoutedEventArgs e)
        {
            window.WindowStyle = WindowStyle.None;
            window.ResizeMode = ResizeMode.NoResize;
            window.WindowState = WindowState.Maximized;


            titleTextBlock.Height = window.Height / 12;
            titleTextBlock.Width = window.Width / 4;
            descriptionTextBox.Height = window.Height / 1.5;
            descriptionTextBox.Width = window.Width / 1.5;
        }
        /// <summary>
        /// Event obsługujący Fullscreen okna (Wyłączenie trybu Fullscreen)
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key == Key.Escape)
            {
                window.WindowStyle = WindowStyle.SingleBorderWindow;
                window.ResizeMode = ResizeMode.CanResize;
                window.WindowState = WindowState.Normal;

                titleTextBlock.Height = window.Height / 12;
                titleTextBlock.Width = window.Width / 4;
                descriptionTextBox.Height = window.Height / 1.5;
                descriptionTextBox.Width = window.Width / 1.5;

            }

        }




    }
}
